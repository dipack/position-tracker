#!/usr/bin/env python3
import sqlite3 as sql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sqlalchemy as sqla

Base = declarative_base()
Session = sessionmaker()

class User(Base):
    __tablename__ = 'users'
    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String)

    def __repr__(self):
        return "<User(id={0}, name={1}>".format(self.id, self.name)

def sqlalchemy_orm():
    engine = sqla.create_engine("sqlite:///example.db", echo=True)
    if not engine.has_table(User.__tablename__):
        Base.metadata.create_all(engine)
    ex_user = User(name="Dipack")
    Session.configure(bind=engine)
    db_session = Session()
    db_session.add(ex_user)
    print(db_session.query(User).filter_by(name="Dipack").first())
    return


def sqlite3_naked():
    connection = sql.connect("example.db")
    print("Connection:", connection)
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS example (date text, name text, url text, price real)")
    ex = ("2018-08-05",
          "Kotak Standard Multicap Fund - Regular Plan (G)",
          "https://www.moneycontrol.com/mutual-funds/nav/kotak-standard-multicap-fund-regular-plan/MKM311",
          24.31)
    cursor.execute("INSERT INTO example VALUES (?, ?, ?, ?)", ex)
    cursor.execute("SELECT * FROM example")
    out = cursor.fetchall()
    print(out)
    connection.close()
    return


def main():
    # sqlite3_naked()
    sqlalchemy_orm()


if __name__ == '__main__':
    main()

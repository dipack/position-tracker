#!/usr/bin/env python3
import requests
import logging
import sqlite3 as sql

logger = logging.getLogger(__name__)
logging.basicConfig(format='[%(asctime)s] [%(levelname)s] %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p', level=logging.INFO)

def connect_to_db(database="example.db"):
    return sql.connect(database)

def setup_db(connection):
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS example (date text, name text, url text, price real)")
    ex = ("2018-08-05",
          "Kotak Standard Multicap Fund - Regular Plan (G)",
          "https://www.moneycontrol.com/mutual-funds/nav/kotak-standard-multicap-fund-regular-plan/MKM311",
          24.31)
    cursor.execute("INSERT INTO example VALUES (?, ?, ?, ?)", ex)
    cursor.execute("SELECT * FROM example")
    out = cursor.fetchall()
    print(out)
    connection.close()
    return

class Security:
    name = ''
    url = ''
    price = ''

    def __init__(self, name, url):
        self.name = name
        self.url = url
        return

    def retrieve_info(self):
        url = self.url
        return

def main():
    kotak_std_multicap_regular_g = Security(
            "Kotak Standard Multicap Fund - Regular Plan (G)",
            "https://www.moneycontrol.com/mutual-funds/nav/kotak-standard-multicap-fund-regular-plan/MKM311"
            )
    kotak_std_multicap_regular_g.retrieve_info()
    print(kotak_std_multicap_regular_g)
    return

if __name__ == '__main__':
    main()
